/* global __DEV__ */
var NI = require('./NI-core.js');

var _sourceMaps = {};
function addSourceMap(aFileName, aSourceMap) {
  _sourceMaps[aFileName] = aSourceMap;
  if (__DEV__) {
    NI.log("addSourceMap: " + aFileName);
  }
}
exports.addSourceMap = addSourceMap;

function installSourceMapSupport() {
  require('source-map-support').install({
    retrieveSourceMap: function(source) {
      var sm = _sourceMaps[source];
      if (sm) {
        return {
          url: null,
          map: sm
        }
      }
      return null;
    }
  });
}
exports.installSourceMapSupport = installSourceMapSupport;

function requireHookTypescript() {
  installSourceMapSupport();

  var MODULE = require('module');
  var TS = require('typescript');
  var FS = require('fs');

  function compileTypeScript(filename) {
    var o = TS.transpileModule(
      '' + FS.readFileSync(filename),
      {
        compilerOptions: {
          target: TS.ScriptTarget.ES5,
          module: TS.ModuleKind.CommonJS,
          sourceMap: true
        },
        fileName: filename
      }
    );
    var code = o.outputText;
    var sm = o.sourceMapText //.replace('.js","','.ts","');
    addSourceMap(filename, sm);
    return code;
  }

  MODULE._extensions['.ts'] = function(m,filename) {
    if (filename.indexOf('node_modules') > -1) {
      m._compile('' + FS.readFileSync(filename), filename);
    }
    else {
      m._compile(compileTypeScript(filename), filename);
    }
  };
}
exports.requireHookTypescript = requireHookTypescript;
