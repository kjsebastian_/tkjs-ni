var NI = require('./NI-core');
var HashID = require('./HashID');

var Serial = {
  create: function(aSalt,aSectionLen,aMinHashLen) {
    var _new = NI.clone(Serial);
    _new._sectionLen = aSectionLen || 5;
    _new._hashId = HashID.createReadable(aSalt, aMinHashLen || (_new._sectionLen * 4));
    return _new;
  },

  encodeString: function(aString) {
    var encoded = this._hashId.encodeString(aString)
    encoded = NI.stringPadAfter(encoded, "B", NI.roundUp(encoded.length,this._sectionLen));
    encoded = NI.stringInsertEveryN(encoded, this._sectionLen, "-");
    return encoded;
  },

  decodeString: function(aString) {
    aString = aString.replace(/[B-]/g, '');
    return this._hashId.decodeString(aString);
  },

  encodeHex: function(aHex) {
    var encoded = this._hashId.encodeHex(aHex)
    encoded = NI.stringPadAfter(encoded, "B", NI.roundUp(encoded.length,this._sectionLen));
    encoded = NI.stringInsertEveryN(encoded, this._sectionLen, "-");
    return encoded;
  },

  decodeHex: function(aHex) {
    aHex = aHex.replace(/[B-]/g, '');
    return this._hashId.decodeHex(aHex);
  },

  encodeNumber: function(aNumber) {
    var encoded = this._hashId.encodeNumber(aNumber)
    encoded = NI.stringPadAfter(encoded, "B", NI.roundUp(encoded.length,this._sectionLen));
    encoded = NI.stringInsertEveryN(encoded, this._sectionLen, "-");
    return encoded;
  },

  decodeNumber: function(aNumber) {
    aNumber = aNumber.replace(/[B-]/g, '');
    return this._hashId.decodeNumber(aNumber);
  },
};

exports.create = Serial.create;
