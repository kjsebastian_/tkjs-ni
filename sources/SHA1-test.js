/* global FIXTURE, TEST */
var NI = require('./NI');
var SHA1 = require('./SHA1');

FIXTURE("SHA1", function() {
  TEST("abc", function() {
    var sha1 = SHA1.hex("abc");
    NI.print("SHA1('abc'): " + sha1);
    NI.assert.equals("a9993e364706816aba3e25717850c26c9cd0d89d", sha1);

    var encodedSHA1 = NI.b64encodeHex(sha1);
    NI.print("encoded SHA1('abc'): " + encodedSHA1);
    NI.print("decoded SHA1('abc'): " + NI.b64decodeHex(encodedSHA1));
  })
})
