/* global FIXTURE, TEST_ASYNC, STARTUP, SHUTDOWN */
var NI = require('./NI-core');
var STORAGE = require('./Storage');

FIXTURE("Storage", function() {
  STARTUP(function(done) {
    STORAGE.init({
      dir: 'storage/test'
    });
    done();
  });

  SHUTDOWN(function() {
  });

  TEST_ASYNC("set/getItem foobar", function(done) {
    STORAGE.setItem("foo", "bar").then(function() {
      return STORAGE.getItem("foo")
    }).then(function(aValueGet) {
      NI.assert.equals("bar", aValueGet);
      done();
    })
  });

  TEST_ASYNC("set/getItem object", function(done) {
    var obj = {
      abc: 123,
      def: [456,789]
    }

    STORAGE.setItem("object", obj).then(function() {
      return STORAGE.getItem("object")
    }).then(function(aValueGet) {
      NI.assert.equals(123, aValueGet.abc);
      NI.assert.equals(789, aValueGet.def[1]);
      done();
    })
  });

  TEST_ASYNC("set/getItem array", function(done) {
    var arr = [123,{foo:"bar"}];

    STORAGE.setItem("array", arr).then(function() {
      return STORAGE.getItem("array")
    }).then(function(aValueGet) {
      NI.assert.equals(123, aValueGet[0]);
      NI.assert.equals("bar", aValueGet[1].foo);
      done();
    })
  });

  TEST_ASYNC("set/getItem complex key name", function(done) {
    var key = "key+name that/is not a valid file$!@:/\\name";
    STORAGE.setItem(key, "complex key name").then(function() {
      return STORAGE.getItem(key)
    }).then(function(aValueGet) {
      NI.assert.equals("complex key name", aValueGet);
      done();
    })
  });

  TEST_ASYNC("set/getItem multiline key name", function(done) {
    var key = [
      "multi",
      "line",
      "key name"
    ].join('\n');
    STORAGE.setItem(key, "multiline key name").then(function() {
      return STORAGE.getItem(key)
    }).then(function(aValueGet) {
      NI.assert.equals("multiline key name", aValueGet);
      done();
    })
  });

});
