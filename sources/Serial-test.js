/* global FIXTURE, TEST */
var NI = require('./NI');
var Serial = require('./Serial');

FIXTURE("Serial", function() {
  TEST("Serial.encode/decodeString", function() {
    var serial = Serial.create();

    var string = "AB123456";
    var encodedString = serial.encodeString(string);
    NI.println("... serial, encodedString: " + encodedString);
    var decodedString = serial.decodeString(encodedString);
    NI.println("... serial, decodedString: " + decodedString);
    NI.assert.equals(string, decodedString);

    var string = "A123456789";
    var encodedString = serial.encodeString(string);
    NI.println("... serial, encodedString: " + encodedString);
    var decodedString = serial.decodeString(encodedString);
    NI.println("... serial, decodedString: " + decodedString);
    NI.assert.equals(string, decodedString);
  })

  TEST("Serial.encode/decodeHex", function() {
    var serial = Serial.create();

    var hex = "a123456789";
    var encodedHex = serial.encodeHex(hex);
    NI.println("... serial, encodedHex: " + encodedHex);
    var decodedHex = serial.decodeHex(encodedHex);
    NI.println("... serial, decodedHex: " + decodedHex);
    NI.assert.equals(hex, decodedHex);

    var hex = "a647755229fc64c67f6012493ad12fdcfe364659";
    var encodedHex = serial.encodeHex(hex);
    NI.println("... serial, encodedHex: " + encodedHex);
    var decodedHex = serial.decodeHex(encodedHex);
    NI.println("... serial, decodedHex: " + decodedHex);
    NI.assert.equals(hex, decodedHex);
  })

  TEST("Serial.encode/decodeNumber", function() {
    var serial = Serial.create(undefined,undefined,4);

    var number = 789123456789;
    var encodedNumber = serial.encodeNumber(number);
    NI.println("... serial, encodedNumber: " + encodedNumber);
    var decodedNumber = serial.decodeNumber(encodedNumber);
    NI.println("... serial, decodedNumber: " + decodedNumber);
    NI.assert.equals(number, decodedNumber);

    var number = 110478;
    var encodedNumber = serial.encodeNumber(number);
    NI.println("... serial, encodedNumber: " + encodedNumber);
    var decodedNumber = serial.decodeNumber(encodedNumber);
    NI.println("... serial, decodedNumber: " + decodedNumber);
    NI.assert.equals(number, decodedNumber);

    var number = 7719;
    var encodedNumber = serial.encodeNumber(number);
    NI.println("... serial, encodedNumber: " + encodedNumber);
    var decodedNumber = serial.decodeNumber(encodedNumber);
    NI.println("... serial, decodedNumber: " + decodedNumber);
    NI.assert.equals(number, decodedNumber);
  })
})
